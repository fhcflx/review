# README #

Arquivos de revisões realizadas para revistas acadêmicas. Cheque também meu perfil no Publons.

### Qual a função deste repositório? ###

* Acesso à minhas revisões para pares.
* Versionamento.
* [Aprenda Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Como utilizo as informações? ###

* Procure por periódicos ou anos específicos
* Observe os arquivos na sua pastas selecionadas
* Cheque os arquivos identificamos como revisões 
* Obedeça a licença de re-uso. Atente para o fato de que a licença cobre o texto de minha se revisões, mas não as réplicas dos autores e editores.
* Reutilize de forma responsável, você assume as consequências pelo uso que fizer da informação

### Contribuições: ###

* Abra pendências (issues)
* Comente à vontade (sigo o código de conduta)
* Acrescente suas próprias revisões 

### Fale com o autor: ###

@fhcflx 